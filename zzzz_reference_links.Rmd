# (PART) References {-}

# References

```{r echo=FALSE, message=FALSE, warning=FALSE, results='hide'}
source("shared_settings_packages.R")
```

- **Statutes**
    - *These will need to be built manually, at least for now*

- **MSBA Policies**
    - *These will need to be built manually, at least for now*



```{r}
makeLabels <- function(x){
  x %>%
    gsub("-", " ", .) %>%
    gsub("[^A-Za-z0-9 _.]", "",.) %>%
    gsub(" ", "-", .) %>%
    gsub("^[:punct:.0-9\\-]+", "", .) %>%
    tolower() %>%
    paste0(".html")
}

found_policies <-
  list.files(".", "Rmd$") %>%
  grep("_partLabel.Rmd$", ., invert = TRUE, value = TRUE) %>%
  grep("^zzzz", ., invert = TRUE, value = TRUE) %>%
  grep("^index.Rmd$", ., invert = TRUE, value = TRUE) %>%
  setNames(str_sub(word(., sep = "_"), 2)) %>%
  sapply(function(x){
    readLines(x)[1] %>%
      gsub("# [0-9]+\\. ", "", .) %>%
      gsub(" \\{#pol[0-9]+\\}$", "", .) %>%
      {.[1]}
  })

found_policies %>%
  paste0("(ref:pol"
         , names(.)
         , ") "
         , " ["
         , "Policy "
         , names(.)
         , " (*"
         , .
         , "*)](#pol"
         , names(.)
         , ")") %>%
  cat(sep = "\n\n")
```



- **Broken School Policy Links**
    - These policies are linked to from another policy,
      but are not found in the list of policies.


```{r}
allLinkedPol <-
  list.files(".", "Rmd$") %>%
  grep("_partLabel.Rmd$", ., invert = TRUE, value = TRUE) %>%
  grep("^zzzz", ., invert = TRUE, value = TRUE) %>%
  grep("^index.Rmd$", ., invert = TRUE, value = TRUE) %>%
  lapply(function(x){
    readLines(x) %>%
      grep("\\(ref:pol", ., value = TRUE) %>%
      grep("^[ \t]+grep", ., invert = TRUE, value = TRUE) %>%
      grep("^[ \t]+paste0", ., invert = TRUE, value = TRUE)
  }) %>%
  unlist() %>%
  str_split("\\(|\\)") %>%
  unlist() %>%
  grep("^ref:pol[0-9]+$", ., value = TRUE) %>%
  paste0("(", . ,")") %>%
  unique() %>%
  sort

allLinkedPol[!(parse_number(allLinkedPol) %in% parse_number(names(found_policies)))] %>%
  paste("    -", .) %>%
  cat(sep = "\n")

cat("\n\n")

allLinkedPol[!(parse_number(allLinkedPol) %in% parse_number(names(found_policies)))] %>%
  paste0(.
         , " *Policy "
         , parse_number(.)
         , " (Not Found)*") %>%
  cat(sep = "\n\n")



```

