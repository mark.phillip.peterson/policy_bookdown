# 2000. Legal Status of the School District {#pol2000}

```{r echo=FALSE, message=FALSE, warning=FALSE, results='hide'}
# Load shared settings and packages
source("shared_settings_packages.R")
```


I.  PURPOSE
    - It is a primary principle of this nation that the public welfare demands an educated and
      informed citizenry. The power to provide for public education is a state function vested in
      the state legislature and delegated to local school districts.
      The purpose of this policy is to
      clarify the legal status of the New London-Spicer School District.

I.  GENERAL STATEMENT OF POLICY
    A.  The school district is a public corporation subject to the control of the legislature,
    limited only by constitutional restrictions. The school district has been created for
    educational purposes.
    
    A.  The legislature has authority to prescribe the school district’s powers and
    privileges, its boundaries and territorial jurisdictions.
    
    A.  The school district has only the powers conferred on it by the legislature; however,
    the school district’s authority to conduct the business of the school district includes
    implied powers in addition to any specific powers granted by the legislature.



I.  RELATIONSHIP TO OTHER ENTITIES
    A.  The school district is a separate legal entity.
    
    A.  The school district is coordinate with and not subordinate to the county(ies) in
    which it is situated.
    
    A.  The school district is not subservient to municipalities within its territory.


I.  POWERS AND AUTHORITY OF THE SCHOOL DISTRICT
    A.  Funds.
        1. The school district, through its school board, has authority to raise funds for
          the operation and maintenance of its schools, and authority to manage and
          expend such funds, subject to applicable law.
        2. The school district has wide discretion over the expenditure of funds under
          its control for public purposes, subject to the limitations provided by law.
        3. School district officials occupy a fiduciary position in the management and
          expenditure of funds entrusted to them.
    
    A.  Raising Funds.
        1. The school district shall, within the limitations specified by law, provide by
          levy of tax necessary funds for the conduct of schools, payment of
          indebtedness, and all proper expenses.
        2. The school district may issue bonds in accordance with the provisions of
          Minn. Stat. Ch. 475, or other applicable law.
        3. The school district has authority to accept gifts and donations for school
          purposes, subject to applicable law.
    
    A.  Property.
        1. The school district may acquire property for school purposes. It may sell,
          exchange, or otherwise dispose of property which is no longer needed for
          school purposes, subject to applicable law.
        2. The school district shall manage its property in a manner consistent with the
          educational functions of the district.
        3. The school district may permit the use of its facilities for community
          purposes which are not inconsistent with, nor disruptive of, its educational
          mission.
        4. School district officials hold school property as trustees for the use and
          benefit of students, taxpayers and the community.
    
    A.  Contracts.
        1. The school district is empowered to enter into contracts in the manner provided
          by law.
        2. The school district has authority to enter into installment purchases and
          leases with an option to purchase, pursuant to Minn. Stat. § 465.71 or other
          applicable law.
        3. The school district has authority to make contracts with other governmental
          agencies and units for the purchase, lease or other acquisition of equipment,
          supplies, materials, or other property, including real property.
        4. The school district has authority to enter into employment contracts. As a
          public employer, the school district, through its designated representatives,
          shall meet and negotiate with public employees in an appropriate 
          bargaining unit and enter into written collective bargaining agreements with
          such employees, subject to applicable law.
    
    A.  Textbooks, Educational Materials, and Studies.
        1. The school district, through its school board and administrators, has the
          authority to determine what textbooks, educational materials, and studies
          should be pursued.
        2. The school district shall establish and apply the school curriculum.
    
    A.  Actions and Suits.
        1. The school district has authority to sue and to be sued.


I. REFERENCES
    - Related policies not linked in text
        - None

    - Cross References
```{r}
cat(linksOut())
```


    - Policies that link to this policy
```{r}
cat(linksIn())
```


    - MSBA References
        - MSBA Service Manual, Chapter 3, Employee Negotiations
        - MSBA Service Manual, Chapter 13, School Law Bulletin “F” (Contract and
          Bidding Procedures)

    - Legal References
        - Minn. Const. art. 13, § 1
        - Minn. Stat. Ch. 123B. (School Districts, Powers and Duties)
        - Minn. Stat. Ch. 179A (Public Employment Labor Relations)
        - Minn. Stat. § 465.035 (Conveyance or Lease of Land)
        - Minn. Stat. §§ 465.71; 471.345; 471.6161; 471.64 (rights, powers, duties of
          political subdivisions)
        - Minnesota Association of Public Schools v. Hanson, 287 Minn. 415, 178
          N.W.2d 846 (1970)
        - Independent School District No. 581 v. Mattheis, 275 Minn. 383, 147
          N.W.2d 374 (1966)
        - Village of Blaine v. Independent School District No. 12, 272 Minn. 343,
          138 N.W.2d 32 (1965)
        - Huffman v. School Board, 230 Minn. 289, 41 N.W.2d 455 (1950)
        - State v. Lakeside Land Co., 71 Minn. 283, 73 N.W.970 (1898)


Adopted 3/24/86    
Revised 6/23/03

